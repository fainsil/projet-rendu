
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.ImageIO;

/**
 * 2D Texture class.
 */
public class Texture {
    int width;
    int height;
    BufferedImage image;

    /**
     * Constructs a new Texture with the content of the image at @path.
     */
    public Texture(String path) throws Exception {
        image = ImageIO.read(new File(path));
        width = image.getWidth();
        height = image.getHeight();
    }

    /**
     * Samples the texture at texture coordinates (u,v), using nearest neighboor
     * interpolation
     * u and v and wrapped around to [0,1].
     */
    public Color sample(double u, double v) {

        double x = u * width;
        double y = v * height;

        int px = (int) Math.floor(x);
        int py = (int) Math.floor(y);

        Color couleur = new Color(image.getRGB(px, py));

        return couleur;
    }
}
